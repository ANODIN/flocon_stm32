# Flocon

This is a school project realized during my studies at ESEO.

Flocon for (*flo*wer *con*nected), is a connected flower pot that takes care of your plant for you.

## Features:
- take measurements on the environment
- watering of the plant
- display of information on a screen
- bluetooth connection

## Components:
- stm32
- HC-05
- DHT11
- BH1705FVI
- TFT screen
- pump