#include <macro_types.h>
#include <stm32f1_uart.h>
#include <stm32f1_rtc.h>
#include "ui/menu.h"
#include "io/sensor.h"

#define SCREEN_DISPLAY_TIME 				20 * 1000
#define ACTIVER_LA_VEIL						1

typedef enum{
		INIT,
		DISPLAY_ON,
		DISPLAY_OFF,
	} fsm_state;

static volatile uint16_t t_screen_timeleft = 0;

static volatile fsm_state fsm_main_state = INIT;
static volatile bool_e fsm_main_entrance = FALSE;

/**
 * @brief Calling every ms extends the screen when the time is reached
 */
void process_timeout_screen_ms (void){
	if(t_screen_timeleft < SCREEN_DISPLAY_TIME) {
		t_screen_timeleft++;
		return;
	}

	fsm_main_state = DISPLAY_OFF;
	fsm_main_entrance = FALSE;
	t_screen_timeleft = 0;
	Systick_remove_callback_function(&process_timeout_screen_ms);
}

/**
 * @brief Main state machine
 */
void fsm_main (void){
	switch(fsm_main_state){
		case INIT:
			BSP_GPIO_PinCfg(LED_GREEN_GPIO, LED_GREEN_PIN, GPIO_MODE_OUTPUT_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH);

			RTC_init(FALSE);
			SENSOR_init();

			PUMP_init();

			BLUETOOTH_init();
			MENU_init();

			Systick_add_callback_function(&SENSOR_process);
			fsm_main_state = DISPLAY_ON;
			break;

		case DISPLAY_ON:
			if(!fsm_main_entrance){
				fsm_main_entrance = TRUE;
				MENU_onoff_led_screen(TRUE);
				if(ACTIVER_LA_VEIL) Systick_add_callback_function(&process_timeout_screen_ms);
			}

			BLUETOOTH_fsm();

			MENU_fsm_navigation(&t_screen_timeleft);
			break;

		case DISPLAY_OFF:
			if(!fsm_main_entrance){
				fsm_main_entrance = TRUE;
				MENU_onoff_led_screen(FALSE);
				MENU_fsm_clean();
			}

			BLUETOOTH_fsm();

			if(MENU_get_navigation_button_state()){
				fsm_main_state = DISPLAY_ON;
				fsm_main_entrance = FALSE;
			}
			break;
	}
}

int main(void)
{
	HAL_Init();
	Systick_init();

	while(1){
		fsm_main();
	}
}
