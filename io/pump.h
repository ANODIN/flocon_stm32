#ifndef IO_PUMP_H_
#define IO_PUMP_H_

#include <macro_types.h>

#define PUMP_PIN			GPIO_PIN_14
#define PUMP_GPIO			GPIOB

void PUMP_init (void);
void PUMP_start (uint16_t water_quantity);

#endif /* IO_PUMP_H_ */
