#include "io/pump.h"
#include <stm32f1xx_hal.h>

static volatile uint16_t t_pump = 0;

void PUMP_wait_for_stop_process(void);

/**
 * @brief Allows you to initialize the pump
 */
void PUMP_init (void){
	BSP_GPIO_PinCfg(PUMP_GPIO, PUMP_PIN, GPIO_MODE_OUTPUT_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH);
}

/**
 * @brief Allows to start the pump and to calculate the time when it must stop according to the quantity of water requested
 * @param water_quantity amount of water required
 * Measurements made (for 7V) :
 * 		5.10 s 	for 14 ml (empty pipe)
 * 		10.30 s for 49ml (pipe already filled)
 */
void PUMP_start (uint16_t water_quantity){
	t_pump = ((uint32_t)((uint32_t)(water_quantity * 5100) / 14));
	if(t_pump > 0){
		Systick_add_callback_function(&PUMP_wait_for_stop_process);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, TRUE);
	}
}

/**************************************************************
 * PRIVATE
 **************************************************************/

/**
 * @brief Allows to stop the pump when the activation time is reached
 */
void PUMP_wait_for_stop_process(void){
	if(t_pump == 0){
		HAL_GPIO_WritePin(PUMP_GPIO, PUMP_PIN, FALSE);
		Systick_remove_callback_function(&PUMP_wait_for_stop_process);
		return;
	}

	t_pump--;
}
