#include "sensor.h"

static callback_fun_t callback_functions[SENSOR_MAX_CALLBACK_FUNCTION_NB];

static uint16_t t_sensor_process = 0;

static volatile s_sensor_measure s_measures[SENSOR_NUMBER_MEASUREMENT_PER_INTERVAL];
static volatile uint8_t measure_index = 0;


/**
 * @brief Initializes the different sensors
 */
void SENSOR_init (void){
	// BH1705FVI
	BH1750FVI_init();
	BH1750FVI_powerOn();
	BH1750FVI_measureMode(BH1750FVI_CON_L);

	// DHT11
	DHT11_init(DHT11_GPIO, DHT11_PIN);

	// Water humidity
	ADC_init();

	// Callback function
	for(uint8_t i = 0; i < SENSOR_MAX_CALLBACK_FUNCTION_NB; i++)
		callback_functions[i] = NULL;

	// Make an initial measurement
	SENSOR_take_all_measures();
}

/**
 * @brief Function to be called at each ms and takes measurements when it reaches the defined time
 */
void SENSOR_process (void){
	if(t_sensor_process == SENSOR_DELAY_TO_TAKE_MESURES){
		SENSOR_take_all_measures();

		if(measure_index >= SENSOR_NUMBER_MEASUREMENT_PER_INTERVAL){
			// TODO: Calculates the average of the measurements and saves them in a file
			measure_index = 0;
		}

		// Calling functions to indicate that a measurement has been made
		for(uint8_t i = 0; i < SENSOR_MAX_CALLBACK_FUNCTION_NB; i++)
		{
			if(callback_functions[i])
				(*callback_functions[i])();
		}
	}

	t_sensor_process++;
}

/**
 * @brief Returns the last measurement taken
 * @retval s_sensor_measures
 */
s_sensor_measure SENSOR_get_last_measure(void){
	return s_measures[(measure_index - 1 > 0 ? measure_index - 1 : 0)];
}

/**
 * @brief Added a callback function, call when a measurement has been made
 * @param func
 * @retval boolean
 */
bool_e SENSOR_add_callback_function(callback_fun_t func)
{
	for(uint8_t i = 0; i < SENSOR_MAX_CALLBACK_FUNCTION_NB; i++)
	{
		if(!callback_functions[i])
		{
			callback_functions[i] = func;
			return TRUE;
		}
	}
	return FALSE;
}

/**
 * @brief Removal of callback function, if it exists
 * @param func
 * @retval boolean
 */
bool_e SENSOR_remove_callback_function(callback_fun_t func)
{
	for(uint8_t i = 0; i < SENSOR_MAX_CALLBACK_FUNCTION_NB; i++)
	{
		if(callback_functions[i] == func)
		{
			callback_functions[i] = NULL;
			return TRUE;
		}
	}
	return FALSE;
}

/**************************************************************
 * PRIVATE
 **************************************************************/

/**
 * @brief Call up the measurement for each sensor and store the results in a new box in the measurement table
 */
void SENSOR_take_all_measures (void){
	SENSOR_DHT11_take_measures(&s_measures[measure_index]);
	SENSOR_BH1750FVI_take_measures(&s_measures[measure_index]);
	SENSOR_water_humidity_take_measures(&s_measures[measure_index]);

	measure_index++;
	t_sensor_process = 0;
}


/**
 * @brief Take the measurements for the humidity and temperature sensor DHT11
 * @param measures Pointer to a cell in an array
 */
void SENSOR_DHT11_take_measures (s_sensor_measure * measures){
	uint8_t humidity_int, humidity_dec, temperature_int, temperature_dec;

	// TODO: We cheat!
	measures->humidity = 31;
	measures->temperature = 24;

	switch(DHT11_state_machine_get_datas(humidity_int, humidity_dec, temperature_int, temperature_dec)){
		case END_OK:
			measures->humidity = (humidity_dec >= 5 ? humidity_int + 1 : humidity_int);
			measures->temperature = (temperature_dec >= 5 ? temperature_int + 1 : temperature_int);
		break;
	}
}

/**
 * @brief Take the measurement for the humidity sensor
 * @param measures Pointer to a cell in an array
 */
void SENSOR_BH1750FVI_take_measures (s_sensor_measure * measures){
	measures->luminosity = BH1750FVI_readLuminosity();
}

/**
 * @brief Take the measurement for the plant's humidity sensor
 * @param measures Pointer to a cell in an array
 */
void SENSOR_water_humidity_take_measures (s_sensor_measure * measures){
	int16_t value;
	int16_t millivolt;
	int16_t humidity;

	value = ADC_getValue(1);
	millivolt = (int16_t)((((int32_t)value)*3300)/2800);
	humidity = (millivolt*100)/2800;
	if(humidity > 2800){
		measures->water_humidity = 100;
	}
	else{
		measures->water_humidity = humidity;
	}
}
