#include "bluetooth.h"
#include "io/sensor.h"

#include <tft_ili9341/stm32f1_ili9341.h>

// Value of the characters of the ASCII table
// used for the delimitation of the transmission
#define SOH 0x1		// start of heading
#define STX 0x2		// start of text
#define ETX 0x3		// end of text
#define EOP 0x4		// end of transmission

#define BLUETOOTH_BUFFER_SIZE			255

static uint8_t bl_buffer[BLUETOOTH_BUFFER_SIZE];
static uint8_t bl_buffer_index = 0;
static bool_e bl_is_connected = FALSE;

static volatile bool_e bl_send_measure = TRUE;

void BLUETOOTH_CMD (void);

// List of possible commands
typedef enum{
	BL_CMD_TIME_AND_DATE = 0x6,
	BL_CMD_PLANT_DATA = 0x7,
	BL_CMD_SET_WATER = 0x8,
	BL_CMD_SEND_MEASURES = 0x9,
} e_bl_command;

typedef enum{
	BL_INIT,
	BL_LISTEN,
	BL_RECEIVE_DATA,
	BL_TRANSLATE,
} e_bluetooth_state;

// TODO: /!\ Temporary - use the pin state of the HC-05 module to update the state
static volatile e_bluetooth_state bl_state = BL_INIT;

/**
 * @brief Initialize Bluetooth
 */
void BLUETOOTH_init(void){
	UART_init(BL_UART, 115200);
	// SYS_set_std_usart(BL_UART, BL_UART, BL_UART);

#if NUCLEO
	// Bluetooth
	UART_init(UART1_ID, 115200);
	SYS_set_std_usart(UART2_ID, UART2_ID, UART2_ID);
#endif

	bl_state = BL_LISTEN;
}

/**
 * @brief Stores the received bit until the end of transmission bit, then processes the request
 */
void BLUETOOTH_fsm (void){
	switch(bl_state){
		case BL_LISTEN:
			if(UART_data_ready(BL_UART) && UART_get_next_byte(BL_UART) == SOH){
				if(!bl_is_connected) bl_is_connected = TRUE;
				bl_state = BL_RECEIVE_DATA;
			}
			break;

		case BL_RECEIVE_DATA:
			if(UART_data_ready(BL_UART))
			{
				uint8_t c = UART_get_next_byte(BL_UART);
				if(c == EOP){
					bl_state = BL_TRANSLATE;
				}else{
					bl_buffer[bl_buffer_index] = c;
					bl_buffer_index++;
				}
			}
			break;

		case BL_TRANSLATE:
			BLUETOOTH_CMD();
			bl_state = BL_LISTEN;
			bl_buffer_index = 0;
			break;
	}
}

/**
 * @brief Allows sending of measurements (not used)
 */
void BLUETOOTH_send_measures (void){
	if(!bl_send_measure) return;

	s_sensor_measure measures = SENSOR_get_last_measure();

	uint8_t buffer[8] = {
			SOH,
			BL_CMD_SEND_MEASURES,
			STX,
			measures.luminosity,
			measures.humidity,
			measures.temperature,
			measures.water_humidity,
			EOP
	};

	UART_puts(BL_UART, buffer, 8);
}

/**
 * @brief Lets you know if a Bluetooth connection has been established
 * @retval bool_e
 */
bool_e BLUETOOTH_get_is_connected (void){
	// TODO: /!\ Temporary
	return bl_is_connected;
}

/**
 * @breif Allows to return Bluetooth data to the computer's serial link (to be executed on the Nucleo)
 *
 * Bluetooth	==> UART 1
 * Serial		==> UART 2
 *
 * RX	==>	PB6
 * TX	==>	PB7
 */
void BLUETOOTH_TEST_echo(void){
#if NUCLEO
	BLUETOOTH_init();

	char buffer[255];
	uint8_t index = 0;
	uint8_t c;

	while(UART_data_ready(UART1_ID)){
		uint8_t c = UART_get_next_byte(UART1_ID);
		if(sprintf(buffer, "%x\r\n", c))
			UART_puts(BL_UART, buffer, 3 * 8);
		buffer[index] = UART_getc(UART1_ID);

		if(buffer[index] == '\n'){
			buffer[index] = '\0';
			index = 0;

			UART_puts(BL_UART, buffer, index);
		}else {
			index++;
		}
	};
	}
#endif
}

/**************************************************************
 * PRIVATE
 **************************************************************/

void BLUETOOTH_CMD (void){
	uint8_t index;

	switch(bl_buffer[0]){
		case BL_CMD_TIME_AND_DATE:
			index = 2;
			RTC_TimeTypeDef sTime;
			sTime.Hours = bl_buffer[index];
			sTime.Minutes = bl_buffer[index + 1];
			sTime.Seconds = bl_buffer[index + 2];

			RTC_DateTypeDef sDate;
			sDate.Date = bl_buffer[index + 3];
			sDate.Month = bl_buffer[index + 4];

			RTC_set_time_and_date(&sTime, &sDate);
			//MENU_update_header();
			break;
		case BL_CMD_SET_WATER:
			index = 2;
			PUMP_start((uint16_t)bl_buffer[index]);
			break;
		case BL_CMD_PLANT_DATA:
			// Retrieve the title
			index = 6;
			uint8_t title_size = bl_buffer[index];
			uint8_t title[title_size + 1];
			for(uint8_t i = (index + 1), ti = 0; ti < title_size && i < bl_buffer_index; i++, ti++){
				title[ti] = bl_buffer[i];
			}

			title[title_size] = '\0';
			break;
	}
}
