#ifndef SENSOR_H_
#define SENSOR_H_

#include <BH1750FVI/bh1750fvi.h>
#include <DHT11/dht11.h>
#include <macro_types.h>
#include <systick.h>

#define DHT11_GPIO	GPIOA
#define DHT11_PIN	GPIO_PIN_0

#define SENSOR_NUMBER_MEASUREMENT_PER_INTERVAL		5
#define SENSOR_MAX_CALLBACK_FUNCTION_NB				2
#define SENSOR_DELAY_TO_TAKE_MESURES				2000
typedef struct {
	uint16_t luminosity;

	uint8_t humidity;
	uint8_t temperature;

	int16_t water_humidity;
} s_sensor_measure;

void SENSOR_init (void);
void SENSOR_process (void);

s_sensor_measure SENSOR_get_last_measure(void);

bool_e SENSOR_remove_callback_function(callback_fun_t func);
bool_e SENSOR_add_callback_function(callback_fun_t func);

#endif /* SENSOR_H_ */
