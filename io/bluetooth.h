/*
 * bluetooth.h
 *
 *  Created on: 11 janv. 2022
 *      Author: Alexa
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include <stm32f1_uart.h>

#define BL_UART				UART2_ID

void BLUETOOTH_TEST_echo(void);

void BLUETOOTH_init(void);
void BLUETOOTH_fsm (void);
bool_e BLUETOOTH_get_is_connected (void);
void BLUETOOTH_send_measures (void);

#endif /* BLUETOOTH_H_ */
