#include <macro_types.h>
#include <stm32f1_rtc.h>
#include <tft_ili9341/stm32f1_ili9341.h>

#include "io/sensor.h"
#include "graph.h"
#include "menu.h"

typedef enum{
	INIT_MENU,
	HOME_MENU,
	MEASURE_MENU,
} e_menu_fsm;

static e_menu_fsm fsm_menu_navigation_state = INIT_MENU;
static bool_e fsm_menu_navigation_entrance = FALSE;

static s_graph_data humidity_graph;
static s_graph_data luminosity_graph;
static s_graph_data temperature_graph;
static s_graph_data water_humidity_graph;

void MENU_update (void);

/**
 * @brief Initializes the menu and its parameters
 * 		O_____________240
 * 		|             |
 * 		| °-----> x   |
 * 		| |           |
 * 		| |           |
 * 		| !           |
 * 		| y           |
 * 		|             |
 * 		|             |
 * 		|             |
 * 		320------------
 * 		 * * * * * * * <-- pins
 */
void MENU_init(void){
	// Screen on/off led
	BSP_GPIO_PinCfg(SCREEN_ONOFF_LED_GPIO, SCREEN_ONOFF_LED_PIN, GPIO_MODE_OUTPUT_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH);
	HAL_GPIO_WritePin(SCREEN_ONOFF_LED_GPIO, SCREEN_ONOFF_LED_PIN, TRUE);

	// TFT Screen
	ILI9341_Init();
	ILI9341_Rotate(ILI9341_Orientation_Portrait_1);

	// Button
	BSP_GPIO_PinCfg(MENU_NAVIGATION_BUTTON_GPIO, MENU_NAVIGATION_BUTTON_PIN, GPIO_MODE_INPUT, GPIO_PULLUP, GPIO_SPEED_FREQ_HIGH);
}

/**
 * @brief Initialize and display the selected menu
 * @param t_screen_timeleft pointer to the screen shutdown timer
 */
void MENU_fsm_navigation (uint16_t * t_screen_timeleft){
	switch(fsm_menu_navigation_state){
		case INIT_MENU:
			if(!fsm_menu_navigation_entrance){
				ILI9341_Fill(MENU_DEFAULT_BACKGROUND_COLOR);
				MENU_show_header();

				fsm_menu_navigation_state = HOME_MENU;
			}
			break;

		case HOME_MENU:
			if(!fsm_menu_navigation_entrance){
				MENU_show_home();
				//MENU_update();

				fsm_menu_navigation_entrance = TRUE;
				SENSOR_add_callback_function(&MENU_update);
			}

			if(MENU_get_navigation_button_state()){
				SENSOR_remove_callback_function(&MENU_update);
				*t_screen_timeleft = 0;
				fsm_menu_navigation_state = MEASURE_MENU;
				fsm_menu_navigation_entrance = FALSE;
			}
			break;

		case MEASURE_MENU:
			if(!fsm_menu_navigation_entrance){
				MENU_show_measure();
				fsm_menu_navigation_entrance = TRUE;
				SENSOR_add_callback_function(&MENU_update);
			}

			if(MENU_get_navigation_button_state()){
				SENSOR_remove_callback_function(&MENU_update);
				*t_screen_timeleft = 0;
				fsm_menu_navigation_state = HOME_MENU;
				fsm_menu_navigation_entrance = FALSE;
			}
			break;
	}
}

/**
 * @brief Refreshes the header display
 */
void MENU_update_header (void){
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	RTC_get_time_and_date(&sTime, &sDate);

	// Time
	char * buff[12];
	if(sprintf(buff, "%u:%u", sTime.Hours, sTime.Minutes) > 0)
		ILI9341_Puts(92, 0, buff, &Font_11x18, ILI9341_COLOR_WHITE, MENU_DEFAULT_BACKGROUND_COLOR);
	else
		ILI9341_Puts(92, 0, "--:--", &Font_11x18, ILI9341_COLOR_WHITE, MENU_DEFAULT_BACKGROUND_COLOR);

	// Date
	if(sprintf(buff, "%u/%u", sDate.Date, sDate.Month) > 0)
		ILI9341_Puts(0, 0, buff, &Font_11x18, ILI9341_COLOR_WHITE, MENU_DEFAULT_BACKGROUND_COLOR);
	else
		ILI9341_Puts(0, 0, "--/--", &Font_11x18, ILI9341_COLOR_WHITE, MENU_DEFAULT_BACKGROUND_COLOR);

	if(BLUETOOTH_get_is_connected()){
		uint8_t icon[] = BL_ICON;
		ILI9341_putImage_monochrome(ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK, 219, 0, 16, 16, &icon, 256);
	}else{
		ILI9341_DrawFilledRectangle(219, 0, 219 + 16, 16, ILI9341_COLOR_BLACK);
	}
}

/**
 * @brief Resets the state machine to its default settings
 */
void MENU_fsm_clean (void){
	clean_menu_window();
	fsm_menu_navigation_state = INIT_MENU;
	fsm_menu_navigation_entrance = FALSE;
	SENSOR_remove_callback_function(&MENU_update);
}

/**************************************************************
 * PRIVATE
 **************************************************************/

/**
 * @brief Update the information on the screen according to the selected menu
 */
void MENU_update (void){

	char buffer [64];
	s_sensor_measure s_last_measure;

	switch(fsm_menu_navigation_state){
		case HOME_MENU:
				MENU_update_header();

				s_last_measure = SENSOR_get_last_measure();
				if(s_last_measure.water_humidity < 30 || s_last_measure.luminosity == 0){
					uint16_t icons4[] = MENU_SAD_ICON;
					ILI9341_putImage(88, 100, 64, 64, icons4, 64*64);
				}else if(s_last_measure.water_humidity < 50 || s_last_measure.luminosity < 5){
					uint16_t icons4[] = MENU_NEUTRAL_ICON;
					ILI9341_putImage(88, 100, 64, 64, icons4, 64*64);
				}else{
					uint16_t icons4[] = MENU_HAPPY_ICON;
					ILI9341_putImage(88, 100, 64, 64, icons4, 64*64);
				}

				if(sprintf(buffer, "%d lx", SENSOR_get_last_measure().luminosity) > 0)
					ILI9341_Puts(24, 287, buffer, &Font_11x18, ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK);

				if(sprintf(buffer, "%d C", SENSOR_get_last_measure().temperature) > 0)
					ILI9341_Puts(104, 287, buffer, &Font_11x18, ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK);

				if(sprintf(buffer, "%d %%", SENSOR_get_last_measure().humidity) > 0)
					ILI9341_Puts(184, 287, buffer, &Font_11x18, ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK);
			break;

		case MEASURE_MENU:
				s_last_measure = SENSOR_get_last_measure();

				// humidity
				GRAPH_plot(&humidity_graph, s_last_measure.water_humidity, ILI9341_COLOR_WHITE);

				// luminosity
				GRAPH_plot(&luminosity_graph, s_last_measure.luminosity, ILI9341_COLOR_WHITE);

				// temperature
				GRAPH_plot(&temperature_graph, s_last_measure.temperature, ILI9341_COLOR_WHITE);

				// humidity of the plant
				GRAPH_plot(&water_humidity_graph, s_last_measure.water_humidity, ILI9341_COLOR_WHITE);
			break;
	}
}

/**
 * @brief Initializes the display of the header
 */
void MENU_show_header (void){
	ILI9341_DrawLine(0, MENU_HEADER_HEIGHT - 1, 240, MENU_HEADER_HEIGHT - 1, ILI9341_COLOR_WHITE);
	MENU_update_header();
}

/**
 * @brief Initialize the display of the home page
 */
void MENU_show_home (){
	clean_menu_window();

	uint8_t icons1[] = SUN_ICON;
	uint8_t icons2[] = TEMPERATURE_ICON;
	uint8_t icons3[] = HUMIDITY_ICON;
	ILI9341_putImage_monochrome(ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK, 24, 247, 32, 32, &icons1, 1024);
	ILI9341_putImage_monochrome(ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK, 104, 247, 32, 32, &icons2, 1024);
	ILI9341_putImage_monochrome(ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK, 184, 247, 32, 32, &icons3, 1024);
}

/**
 * @brief Initializes the display of the measurements menu
 */
void MENU_show_measure (void){
	clean_menu_window();

	humidity_graph = GRAPH_init(0, 50, 120, 150, 0, 100, "humidity");
	luminosity_graph = GRAPH_init(135, 50, 240, 150, 0, 100, "luminosity");
	temperature_graph = GRAPH_init(0, 170, 120, 270, 0, 100, "temperature");
	water_humidity_graph = GRAPH_init(135, 170, 240, 270, 0, 100, "plant humidity");
}

/**
 * @brief Clean display in the menu area
 */
void clean_menu_window (){
	ILI9341_DrawFilledRectangle(0, MENU_HEADER_HEIGHT, 240, 320, MENU_DEFAULT_BACKGROUND_COLOR);
}

/**
 * @brief Allows you to turn the screen's LEDs on or off
 */
void MENU_onoff_led_screen (bool_e isOn){
	if(isOn){
		ILI9341_DisplayOn();
		HAL_GPIO_WritePin(SCREEN_ONOFF_LED_GPIO, SCREEN_ONOFF_LED_PIN, !isOn);
	}
	else{
		HAL_GPIO_WritePin(SCREEN_ONOFF_LED_GPIO, SCREEN_ONOFF_LED_PIN, !isOn);
		ILI9341_DisplayOff();
	}
}

/**
 * @brief Allows you to retrieve the status of the button
 * @retval bool_e button status
 */
bool_e MENU_get_navigation_button_state (void){
	return !HAL_GPIO_ReadPin(MENU_NAVIGATION_BUTTON_GPIO, MENU_NAVIGATION_BUTTON_PIN);
}
