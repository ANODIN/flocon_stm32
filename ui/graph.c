#include "graph.h"

#include <tft_ili9341/stm32f1_ili9341.h>

/**
 * @brief Initializes a graph with its parameters and displays it on the screen
 * @param start_x
 * @param start_y
 * @param end_x
 * @param end_y
 * @param min
 * @param max
 * @param title
 * @retval s_graph_data structure containing the parameters of the graph
 */
s_graph_data GRAPH_init (uint16_t start_x, uint16_t start_y, uint16_t end_x, uint16_t end_y, uint16_t min, uint16_t max, char * title){
	s_graph_data dt;

	dt.min = min;
	dt.max = max;
	dt.index = 0;

	dt.start_x = start_x + GRAPH_X_OFFSET * 2 + 1 + GRAPH_DOT_RADIUS;
	dt.end_x = end_x;
	dt.end_y = end_y - (GRAPH_X_OFFSET * 2 + GRAPH_DOT_RADIUS);
	dt.offset = (dt.end_x - dt.start_x) / GRAPH_NBR_VALUE;

	// Title
	uint16_t width, height, txt_pos_x, left_offset;
	ILI9341_GetStringSize(title, &Font_7x10, &width, &height);

	left_offset = ((end_x - start_x) - width) / 2;
	txt_pos_x = (left_offset > 0 ? start_x +left_offset : 0);

	ILI9341_Puts(
			txt_pos_x,
			start_y,
			title,
			&Font_7x10,
			ILI9341_COLOR_WHITE,
			ILI9341_COLOR_BLACK);

	dt.start_y = start_y + height;

	// Vertical line
	ILI9341_DrawLine(
			start_x + GRAPH_X_OFFSET + 1,
			start_y,
			start_x + GRAPH_X_OFFSET + 1,
			end_y,
			ILI9341_COLOR_WHITE);


	ILI9341_DrawLine(
			start_x,
			dt.start_y,
			dt.start_x,
			dt.start_y,
			ILI9341_COLOR_WHITE);

	// Horizontal line
	ILI9341_DrawLine(
			start_x,
			end_y - (GRAPH_X_OFFSET + 1),
			end_x,
			end_y - (GRAPH_X_OFFSET + 1),
			ILI9341_COLOR_WHITE);

	for(int i = 1; i < GRAPH_NBR_VALUE; i++){
		uint8_t pos_x = dt.start_x + dt.offset * i;
		ILI9341_DrawLine(
			pos_x,
			dt.end_y,
			pos_x,
			end_y,
			ILI9341_COLOR_WHITE);
	}

	return dt;
}

/**
 * @brief Clears the graph display according to the arguments and resets it
 * @param dt
 * @param withDisplay if true the display is cleaned
 */
void GRAPH_clean (s_graph_data * dt, bool_e withDisplay){
	if(withDisplay){
		ILI9341_DrawFilledRectangle(
			dt->start_x,
			dt->start_y,
			dt->end_x,
			dt->end_y,
			ILI9341_COLOR_BLACK);
	}
	dt->index = 0;
}

/**
 * @brief Plots the value in the graph
 * @param dt
 * @param value
 * @param color
 */
void GRAPH_plot (s_graph_data * dt, uint16_t value, uint16_t color){
	if(dt->index > GRAPH_NBR_VALUE){
		GRAPH_clean(dt, TRUE);

		dt->index = 1;
		dt->old_value = dt->lastest_value;

		GRAPH_plot(dt, value, color);
	}else{
		// Clamp
		value = (value > dt->max) ? dt->max : ((value < dt->min) ? dt->min : value);

		uint16_t pos_y = (dt->end_y * (dt->max - value) + (dt->start_y + GRAPH_DOT_RADIUS) * (value - dt->min)) / (dt->max - dt->min); // Lerp
		uint16_t pos_x = dt->start_x + dt->offset * dt->index;

		dt->old_value = dt->lastest_value;
		dt->lastest_value = pos_y;

		if(dt->index > 0){
			ILI9341_DrawFilledCircle(pos_x, pos_y, GRAPH_DOT_RADIUS, color);
			ILI9341_DrawLine(
					dt->start_x + dt->offset * (dt->index - 1),
					dt->old_value,
					pos_x,
					pos_y,
					color);
		}

		dt->index++;
	}
}
