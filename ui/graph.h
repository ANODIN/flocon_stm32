#ifndef GRAPH_H_
#define GRAPH_H_

#define GRAPH_NBR_VALUE			6
#define GRAPH_X_OFFSET			2 // even number is required
#define GRAPH_DOT_RADIUS		2 // even number is required

#include <macro_types.h>

typedef struct{
	uint16_t start_x, start_y, end_x, end_y;

	uint8_t index, offset;
	uint16_t lastest_value, old_value, min, max;
} s_graph_data;

s_graph_data GRAPH_init (uint16_t start_x, uint16_t start_y, uint16_t end_x, uint16_t end_y, uint16_t min, uint16_t max, char * title);
void GRAPH_plot (s_graph_data * dt, uint16_t value, uint16_t color);
void GRAPH_clean (s_graph_data * dt, bool_e withDisplay);

#endif /* GRAPH_H_ */
